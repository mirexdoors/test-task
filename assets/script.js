function pasteShortUrl(data) {
	var resultDiv = document.createElement('div');
	resultDiv.id = 'result';
	resultDiv.innerHTML = data;
	document.body.appendChild(resultDiv);
}

function sendAjaxRequest(longUrl) {
	var xhr = new XMLHttpRequest();
	var url = '/vendor/handler.php';
	var params = 'longUrl=' + longUrl;
	xhr.open('POST', url, true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			pasteShortUrl(xhr.responseText);
		} else {
			console.log ('Что-то пошло не так');
		}
	}

	xhr.send(params);
}

function getShortUrl(){

	var longUrl = document.getElementById('url').value; //значение input id=url    нужно получать от родителя
	var errorMessage = null; //сообщение об ошибке
	var flag = true; //флаг валидности значения input
	var reg =/^(ftp|http|https):\/\/[^ "]+$/; //регулярное выражение для url

	if (document.getElementById('result'))  document.getElementById('result').remove()  //удаляем предыдущее значение

	if (longUrl.length < 1 || !reg.test(longUrl)) {
		errorMessage = 'Некорректный URL';
		flag = false;
	}

	if (!flag) {
		alert(errorMessage);
	} else {
		sendAjaxRequest(longUrl); //если валидно, отправляем запрос
	}
};

document.addEventListener("DOMContentLoaded", function(event) {
	document.getElementById("submit").onclick = getShortUrl;
});
